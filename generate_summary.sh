MH_RUN=$1

#Define fixed arguments for nit_LOH_pipeline.sh
SAVE_ROOT="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/results"

#Define paths for files needed in this script
FILES_LIST="/mnt/disk4/labs/salipante/nithisha/MH_file_lists/$MH_RUN.txt"
#SUMMARY_FILE="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/nit_code/summary_file_Nit.txt"
SUMMARY_FILE="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/nit_code/new.txt"

generate_summary_file () {
	#Read in the file_list.txt file, line by line and parse it to pass onto $TUMOR_PFX and $NORMAL_PFX
        while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')

                #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
			if [ -f $SAVE_ROOT/$TUMOR_PFX/$TUMOR_PFX.copynumber_calls.txt ];then
				SCORE_FILE="$SAVE_ROOT/$TUMOR_PFX/$TUMOR_PFX.score.txt"
				RUNID=$MH_RUN
				samplename=$TUMOR_PFX
				cellularity=$(sed -n 5p $SCORE_FILE | awk -F': ' '{print $2}')
				ploidy=$(sed -n 6p $SCORE_FILE | awk -F': ' '{print $2}')
				validbases=$(sed -n 1p $SCORE_FILE | awk -F': ' '{print $2}')
				LOH_score=$(sed -n 3p $SCORE_FILE | awk -F ': ' '{print $2}')
				echo -e "$RUNID\t$samplename\t$cellularity\t$ploidy\t$validbases\t$LOH_score" >> $SUMMARY_FILE
		
			else
				RUNID=$MH_RUN
				samplename=$TUMOR_PFX
				cellularity=$(cat $SAVE_ROOT/$TUMOR_PFX/$TUMOR_PFX.cellularity.txt)
				ploidy=$(cat $SAVE_ROOT/$TUMOR_PFX/$TUMOR_PFX.ploidy.txt)
				validbases=0
				LOH_score=0
				echo -e "$RUNID\t$samplename\t$cellularity\t$ploidy\t$validbases\t$LOH_score" >> $SUMMARY_FILE

			fi

		#####################SINGLE SAMPLE##########################
		else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
			#Skip this sample
	
	fi
	done <$FILES_LIST                     
}

generate_summary_file



