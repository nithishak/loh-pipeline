LOH pipeline README
====================

This LOH pipeline is designed to find a LOH score for a paired tumor/germline sample using a) read depth and b) variant allele frequencies.
Only chromosomes in which alleleA and alleleB loss is below 0.75% are considered.

Dependencies
-------------
1. Python 2.7
2. R 3.3
3. BWA 0.7.12
4. Samtools 0.1.19
5. Sequenza 3.0.0
6. Super Deduper

Input files
------------
Note: For paths that need to be indicated, please refer to end of README  
1. A file containing all samples in a run. The file should be named RUN.txt and may contain paired/unpaired samples. Each new unpaired/paired sample should be represented on a new line and the format is tumor_id|germline_id. This would look like:  

* unpaired_tumor_id|
* |unpaired_germline_id
* paired_tumor_id|paired_germline_id 

2. A human reference genome sequence (human_g1k_v37.fasta)  
3. GC content file ((hg19.gc5Base_v4.txt.gz)

How to run the pipeline
------------------------
1. Run LOH wrapper (usually, the number of samples is set to 4)  
``` LOH_wrapper.sh <RUN name> <number of samples to run each round>```
2. Generate summary file  
```generate_summary.sh <RUN name> ```

Intermediate Outputs created
----------------------------
For each paired sample, the following files are produced (.T represents file associated with tumor and .G represents file associated with germline):  

* <sample_id>.G.bam
* <sample_id>.G.bam.bai
* <sample_id>.T.bam
* <sample_id>.T.bam.bai
* <sample_id>.G.mpileup.gz
* <sample_id>.T.mpileup.gz
* <sample_id>.G.log
* <sample_id>.T.log
* <sample_id>.T.binned.seqz.gz
* <sample_id>.T_segments.txt
* <sample_id>.T_chromosome_view.pdf
* <sample_id>.T_mutations.txt
* <sample_id>.T_sequenza_cp_table.RData
* <sample_id>.T_CN_bars.pdf
* <sample_id>.T.ave_depth.txt
* <sample_id>.T_sequenza_extract.RData
* <sample_id>.T_confints_CP.txt
* <sample_id>.T.cellularity.jpg
* <sample_id>.T_sequenza_log.txt
* <sample_id>.T_alternative_fit.pdf
* <sample_id>.T_CP_contours.pdf
* <sample_id>.T.cellularity.txt
* <sample_id>.T.sequenza.r
* <sample_id>.T_alternative_solutions.txt
* <sample_id>.T_genome_view.pdf
* <sample_id>.T.copynumber_calls.txt
* <sample_id>.T.seqz.gz
* <sample_id>.T.ploidy.txt
* <sample_id>.T_model_fit.pdf
* <sample_id>.T.score.txt
 
Final output
------------
When ```generate_summary.sh <RUN name>``` is run, a tab separated file with the following information is generated. Any zeros can be interpreted as the sample not having enough coverage.  
```RunID samplename cellularity ploidy validbases LOH_score```


Paths to modify
----------------
For LOH_wrapper.sh modify paths for:  

* PATH_TO_PIPELINE (path for LOH_pipeline.sh)
* SAVE_ROOT (path to results directory)
* DATA_PATH (path to run directory that holds fastq files)

* FILES_LIST (path to input file that contains samples in a run)
* GENERATE_SUMMARY (path to generate_summary.sh)
* SUMMARY_FILE (path to file where you would like final results stored)

For LOH_pipeline.sh modify paths for:

* BWA
* SAMTOOLS
* SEQUENZAUTILS(path to sequenza-utils.py)
* ALIGNMENT_REF_GENOME (path to human reference genome FASTA file)
* GC_WINDOW (path to hg19.gc5Base_v4.txt.gz)
* SUPER_DEDUPER
* SCORING_SCRIPT_PYTHON (path to LOH_score.py)

For generate_summary.sh modify paths for:

* SAVE_ROOT (path to results directory)
* FILES_LIST (path to input file that contains samples in a run)
* SUMMARY_FILE (path to file where you would like final results stored)

