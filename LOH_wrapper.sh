set -x
#Define arguments passed to LOH_wrapper.sh
MH_RUN=$1
JOBS_IN_BATCH=$2

#Define fixed arguments for LOH_pipeline.sh
PATH_TO_PIPELINE="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/nit_code/LOH_pipeline.sh" 
SAVE_ROOT="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/results"
DATA_PATH="/mnt/disk4/labs/salipante/data/external_runs/Swisher_MH_data/$MH_RUN"
CORES=4

#Define paths for files needed in this script
FILES_LIST="/mnt/disk4/labs/salipante/nithisha/MH_file_lists/$MH_RUN.txt"
GENERATE_SUMMARY="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/nit_code/generate_summary.sh"
SUMMARY_FILE="/mnt/disk4/labs/salipante/nithisha/LOH_Scoring/nit_code/summary_file_Nit.txt"

#create a dir for logs for this mh run
mkdir ${MH_RUN}_logs

############################################################################CHECKS#####################################################################################
#Parameter check
if [ "$#" -ne 2 ]; then
	echo "Not all parameters were provided"
	exit 1
fi

#File check
IFS="|"
FILES="$FILES_LIST|$GENERATE_SUMMARY|$SUMMARY_FILE|$PATH_TO_PIPELINE"
for file in $FILES
	do
	if [ ! -f $file ];then
		echo "ERROR:One of the required main files is missing.Please check" 
		exit 1
	fi
done

#Dir check
DIRS="$SAVE_ROOT|$DATA_PATH|${MH_RUN}_logs"
for dir in $DIRS
do
	if [ ! -d $dir ];then
		echo "ERROR:One of the required directories is missing.Please check"
		exit 1
	fi
done
unset IFS
#######################################################################################################################################################################
#Main code

run_LOH_batches() {
	#initialize a counter
	count=0
	#Read in the file_list.txt file, line by line and parse it to pass onto $TUMOR_PFX and $NORMAL_PFX
	while IFS='' read -r line
	do
		TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
		GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')

		#####################PAIRED SAMPLE##########################
		if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
			#Let us run the command
			nohup $PATH_TO_PIPELINE $TUMOR_PFX $GERMLINE_PFX $SAVE_ROOT $DATA_PATH $CORES >>./${MH_RUN}_logs/${TUMOR_PFX}_nohup.log 2>>./${MH_RUN}_logs/${TUMOR_PFX}_nohup.log &
			count=$((count+1))

		#####################SINGLE SAMPLE##########################
		else
			SAMPLE_PFX=$(echo $line|tr -d '|')
			#Skip this sample

		fi	
	

	if [ $count -gt $JOBS_IN_BATCH ] #$JOBS_IN_BATCH should be 3 if u want 4 jobs running in parallel
	then
	wait
	count=0
	fi
	done < $FILES_LIST
}


run_LOH_batches 


