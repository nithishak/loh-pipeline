import re
import sys

validChromosomes = ","
chromosomesToTest = range(1,23)
chromosomesToTest.append('X')

def validateChromosome(input_file, chromosome, loss_threshold, total_bases_per_C = 0, lossA = 0, lossB = 0 ):
	global validChromosomes
	with open(input_file, "r+") as seq_matrix:
		header = seq_matrix.readline()
		for line in seq_matrix:
			
			segmentLength = 0

			line =  re.sub("\"", "", line)
			line = re.sub("chr", "", line)
			line = line.replace("\n", "")

			outputArray = line.split(" ") #forms a list of strs

			if str(chromosome) == outputArray[1]:
				#print outputArray[1]
				segmentLength = int(outputArray[3]) - int(outputArray[2])
				total_bases_per_C = total_bases_per_C + segmentLength

				if outputArray[11] == "0": #A is 0
					lossA = lossA + segmentLength
				if outputArray[12] == "0": #B is 0
					lossB = lossB + segmentLength
	
	print "\n"
	print "no of allele A bases lost: " + str(lossA)
	print "no of allele B bases lost: " + str(lossB)
	if not total_bases_per_C == 0:
		percentAloss = float(lossA) / total_bases_per_C
		print ("percent_Aloss: %.2f for chromosome %s" %(percentAloss, str(chromosome)))
		percentBloss = float(lossB) / total_bases_per_C
		print ("percent_Bloss: %.2f for chromosome %s" %(percentBloss ,str(chromosome)))
		

		if percentAloss <= float(loss_threshold) and percentBloss <= float(loss_threshold):
			validChromosomes = validChromosomes + str(chromosome) + ","
			#print validChromosomes

def find_score(input_file):
	global total_bases
	global total_lost_bases
	with open(input_file, "r+") as seq_matrix:
		header = seq_matrix.readline()
		for line in seq_matrix:
			segmentLength = 0


			line =  re.sub("\"", "", line)
			line = re.sub("chr", "", line)
			line = line.replace("\n", "")

			outputArray = line.split(" ")

			if outputArray[1] in validChromosomes.split(","):
				segmentLength = int(outputArray[3]) - int(outputArray[2])
				total_bases = total_bases + segmentLength

				if outputArray[11] == "0" or outputArray[12] == "0":
					total_lost_bases = total_lost_bases + segmentLength

def write_to_output_file(output_file):
	with open(output_file, "w+") as output:
		output.write("Total valid bases analyzed: %d\n" %total_bases)
		output.write("Valid lost bases identified: %d\n" %total_lost_bases)
		lossScore = 0

		if not total_lost_bases == 0:
			print "entered loop"
			lossScore = float(total_lost_bases)/total_bases

		print "loss score is " + str(lossScore)
		output.write("Fraction valid bases lost: %.10f\n" %lossScore)
		output.write("Valid chromosomes: %s" %validChromosomes)



#MAIN CODE#
input_file = sys.argv[1]
output_file = sys.argv[2]
loss_threshold = sys.argv[3]

for chromosome in chromosomesToTest:
	validateChromosome(input_file, chromosome, loss_threshold)

print "\n"
print "Final validChromosomes: " + str(validChromosomes)

total_bases = 0
total_lost_bases = 0
find_score(input_file)

print "total_bases: " + str(total_bases)
print "total_lost_bases: " + str(total_lost_bases)

write_to_output_file(output_file)

